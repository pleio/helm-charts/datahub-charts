# Datahub Charts

This repository contains the Helm-chart for [Datahub](https://gitlab.com/pleio/datahub/-/tree/main) (and any future charts which are related to this project). 

## Installation
See [the Datahub Chart's README](./datahub/README.md) for installation instructions

## Chart Releases
Any changes in the chart directories (e.g. [datahub](./datahub/)) triggers a pipeline which publishes a chart package into this project's [package registry](https://gitlab.com/pleio/helm-charts/datahub-charts/-/packages). 

## Development
Please create merge-requests for any additional features or bugfixes. Please do not forget to:
* bump the chart-version in the Chart.yaml (so `version`, `appversion` can stay as is)
* add any additional values in de [Datahub chart README](./datahub/README.md) and run [helm-docs](https://github.com/norwoodj/helm-docs/tree/master) to automatically generate the docs