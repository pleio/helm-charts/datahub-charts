# datahub

![Version: 0.5.5](https://img.shields.io/badge/Version-0.5.5-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

Helm-Chart for Datahub.

## Source Code

* <https://gitlab.com/pleio/helm-charts/datahub-charts>

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| https://charts.bitnami.com/bitnami | redis | 20.7.1 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` | Affinity for pod assignment |
| autoscaling.enabled | bool | `false` | Enable autoscaling for Datahub |
| autoscaling.maxReplicas | int | `100` | Maximum number of Datahub replicas |
| autoscaling.minReplicas | int | `1` | Minimum number of Datahub replicas |
| autoscaling.targetCPUUtilizationPercentage | int | `80` | Target CPU utilization percentage |
| autoscaling.targetMemoryUtilizationPercentage | int | `80` | Target Memory utilization percentage |
| existingSecret | string | `nil` | Name of existing secret that contains all required secrets |
| fullnameOverride | string | `""` | Override the default fully qualified app name |
| image.pullPolicy | string | `"IfNotPresent"` | Default pull policy |
| image.repository | string | `"registry.gitlab.com/pleio/datahub"` | Default image repository |
| image.tag | string | `"latest"` | Overrides the image tag whose default is the chart appVersion. |
| imagePullSecrets | list | `[]` | Secrets used to pull the Datahub images (if required) |
| ingress.annotations | object | `{}` | Annotations on the Ingress objects |
| ingress.className | string | `""` | IngressClass that will be be used to implement the Ingress (Kubernetes 1.18+) |
| ingress.enabled | bool | `false` | Whether to enable Ingress |
| ingress.hosts[0] | object | `{"host":"chart-example.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}` | Default host for the ingress record |
| ingress.hosts[0].paths[0] | object | `{"path":"/","pathType":"ImplementationSpecific"}` | Default path for the ingress record |
| ingress.hosts[0].paths[0].pathType | string | `"ImplementationSpecific"` | Ingress path type |
| ingress.tls | list | `[]` | Enable TLS configuration for the host defined at ingress.hostname parameter |
| livenessProbe.failureThreshold | int | `6` | Failure threshold for the Datahub livenessProbe |
| livenessProbe.initialDelaySeconds | int | `40` | Initial delay seconds for the Datahub livenessProbe |
| livenessProbe.periodSeconds | int | `10` | Period seconds for the Datahub livenessProbe |
| livenessProbe.successThreshold | int | `1` | Success threshold for the Datahub livenessProbe |
| livenessProbe.timeoutSeconds | int | `1` | Timeout seconds for the Datahub livenessProbe |
| nameOverride | string | `""` | Override the chart name |
| nodeSelector | object | `{}` | Node labels for pod assignment |
| podAnnotations | object | `{}` | Additional pod annotations |
| podLabels | object | `{}` | Additional pod labels |
| podSecurityContext | object | `{}` | Set the pod security-context |
| readinessProbe.failureThreshold | int | `6` | Failure threshold for the Datahub readinessProbe |
| readinessProbe.initialDelaySeconds | int | `20` | Initial delay seconds for the Datahub readinessProbe |
| readinessProbe.periodSeconds | int | `10` | Period seconds for the Datahub readinessProbe |
| readinessProbe.successThreshold | int | `1` | Success threshold for the Datahub readinessProbe |
| readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for the Datahub readinessProbe |
| redis.architecture | string | `"standalone"` |  |
| redis.auth.password | string | `""` |  |
| redis.enabled | bool | `true` |  |
| redis.master.persistence.enabled | bool | `true` |  |
| redis.master.persistence.existingClaim | string | `"override"` |  |
| redis.master.resources.limits.cpu | string | `"750m"` |  |
| redis.master.resources.limits.memory | string | `"1G"` |  |
| redis.master.resources.requests.cpu | string | `"500m"` |  |
| redis.master.resources.requests.memory | string | `"500Mi"` |  |
| replicaCount | int | `1` | Amount of replicas used for the Datahub deployments |
| resources | object | `{}` | The resources limits and requests |
| securityContext | object | `{}` | Set the security-context |
| service.port | int | `80` | The port of the service |
| service.type | string | `"ClusterIP"` | Service type (ClusterIP, Loadbalancer or Nodeport) |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `false` | pecifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| settings.azure.accountKey | string | `""` | Access key to Azure Storage Account |
| settings.azure.accountName | string | `""` | Name of Azure Storage Account |
| settings.azure.containerName | string | `""` | Name of container on Azure Storage |
| settings.azure.subscriptionID | string | `""` | ID of the Azure Subscription |
| settings.datahub.allowedHosts | string | `""` | A comma-seperated list of strings representing the host/domain names that may access Datahub |
| settings.datahub.avatarImageBaseUrl | string | `""` | Base URL for avatar images, used for CSP img-src |
| settings.datahub.celeryBrokerUrl | string | `""` | Celery broker Redis URL |
| settings.datahub.corsAllowedOrigins | string | `""` | A comma-seperated list of strings representing the host/domain names that are added to CORS response header |
| settings.datahub.csrfTrustedOrigins | string | `""` | CSRF trusted origins |
| settings.datahub.db.host | string | `""` | postgres host |
| settings.datahub.db.name | string | `""` | postgres db name |
| settings.datahub.db.password | string | `""` | postgres password |
| settings.datahub.db.port | string | `""` | postgres port |
| settings.datahub.db.user | string | `""` | postgres user |
| settings.datahub.djangoAdminUrl | string | `""` | Secret path in the Django-admin URL |
| settings.datahub.environment | string | `"development"` | The type of environment. If left empty, it will default to production |
| settings.datahub.login.redirectUrl | string | `""` | Redirect link after authentication. Useful in a development environment. Defaults to / |
| settings.datahub.login.redirectUrlFailure | string | `""` | Redirect link after login error. Defaults to / |
| settings.datahub.logout.redirectUrl | string | `""` | Redirect link after logging out. |
| settings.datahub.oidc.baseUrl | string | `""` | The base url for the OIDC server. Trailing slash is required |
| settings.datahub.oidc.op.authorizationEndpoint | string | `""` | Authorization endpoint addition to oidc.op.baseUrl |
| settings.datahub.oidc.op.logoutEndpoint | string | `""` | Logout info endpoint addition to oidc.op.authorizationEndpoint |
| settings.datahub.oidc.op.tokenEndpoint | string | `""` | Token endpoint addition to oidc.op.tokenEndpoint |
| settings.datahub.oidc.op.userEndpoint | string | `""` | User endpoint addition to oidc.op.userEndpoint |
| settings.datahub.oidc.rp.clientId | string | `""` | The ID used in the OIDC provider for this client |
| settings.datahub.oidc.rp.clientSecret | string | `""` | The secret used in the OIDC provider for this client |
| settings.datahub.oidc.rp.signAlgo | string | `""` | The signing algorithm |
| settings.datahub.profileUrl | string | `""` | The URL to the OIDC profile page |
| settings.datahub.secretKey | string | `""` | Secret key for the backend app. Not required for development. |
| settings.datahub.staticRoot | string | `"/var/datahub/static"` | The staticroot folder used by collectstatic to copy static files into |
| settings.nginx.livenessProbe.failureThreshold | int | `6` | Failure threshold for the Nginx livenessProbe |
| settings.nginx.livenessProbe.initialDelaySeconds | int | `10` | Initial delay seconds for the Nginx livenessProbe |
| settings.nginx.livenessProbe.periodSeconds | int | `5` | Period seconds for the Nginx livenessProbe |
| settings.nginx.livenessProbe.successThreshold | int | `1` | Success threshold for the Nginx livenessProbe |
| settings.nginx.livenessProbe.timeoutSeconds | int | `1` | Timeout seconds for the Nginx livenessProbe |
| settings.nginx.readinessProbe.failureThreshold | int | `6` | Failure threshold for the Nginx readinessProbe |
| settings.nginx.readinessProbe.initialDelaySeconds | int | `5` | Initial delay seconds for the Nginx readinessProbe |
| settings.nginx.readinessProbe.periodSeconds | int | `5` | Period seconds for the Nginx readinessProbe |
| settings.nginx.readinessProbe.successThreshold | int | `1` | Success threshold for the Nginx readinessProbe |
| settings.nginx.readinessProbe.timeoutSeconds | int | `1` | Timeout seconds for the Nginx readinessProbe |
| settings.nginx.repository | string | `"nginx"` | Container image registry for the Nginx image |
| settings.nginx.resources.limits.cpu | string | `"300m"` | CPU resource limits |
| settings.nginx.resources.limits.memory | string | `"64Mi"` | Memory resource limits |
| settings.nginx.resources.requests.cpu | string | `"100m"` | CPU resource requests |
| settings.nginx.resources.requests.memory | string | `"64Mi"` | Memory resource requests |
| settings.nginx.tag | string | `"1.21-alpine"` | Nginx container image tag |
| settings.worker.resources.limits.cpu | string | `"400m"` | CPU resource limits |
| settings.worker.resources.limits.memory | string | `"1Gi"` | Memory resource limits |
| settings.worker.resources.requests.cpu | string | `"400m"` | CPU resource requests |
| settings.worker.resources.requests.memory | string | `"512Mi"` | Memory resource requests |
| tolerations | list | `[]` | Tolerations for pod assignment |
